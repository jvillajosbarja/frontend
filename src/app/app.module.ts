import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { appRoutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { SettingsComponent } from './settings/settings.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment.prod';
import { CoreModule } from './core/core.module';
import { DocManagementModule } from './docManagement/docManagement.module';


@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent
  ],
  imports: [
    appRoutes,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,  //load primeNg components
    CoreModule,
    DocManagementModule,
    StoreModule.forRoot({})
/*    StoreDevtoolsModule.instrument({
      name: 'DevTools',
      maxAge: 25,
      logOnly: environment.production,
    })*/
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'nl-BE'},
    {provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
