import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { throwIfAlreadyLoaded } from './guards/module-import.guard';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppFooterComponent } from './components/app.footer.component';
import { AppMenuComponent } from './components/app.menu.component';
import { AppSubMenuComponent } from './components/app.sub-menu.component';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { BreadcrumbService } from './services/breadcrumb.service';
import { AppBreadcrumbComponent } from './components/app.breadcrumb.component';
import { GlobalErrorService } from './services/error-handling/global-error.service';
import { GlobalErrorInterceptor } from './interceptors/global-error-interceptor';
import { HttpContentInterceptor } from './interceptors/http-content-interceptor';

export const ultimaComponents = [
  AppBreadcrumbComponent
];

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    AppFooterComponent,
    AppMenuComponent,
    AppSubMenuComponent,
    ultimaComponents
  ],
  exports: [
    AppFooterComponent,
    AppMenuComponent,
    AppSubMenuComponent,
    ultimaComponents
  ],
  providers: [
    BreadcrumbService,
    GlobalErrorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GlobalErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpContentInterceptor,
      multi: true
    }
  ]
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

}
