import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class GlobalErrorService {

  private errorSubject: Subject<any> = new Subject<any>();

  private lastErrorReceived: HttpErrorResponse;
  private lastErrorReceivedTimestamp: number;

  constructor() { }

  getError(): Observable<any> {
    return this.errorSubject.asObservable();
  }

  handleError(error: any): void {
    if (error instanceof HttpErrorResponse) {

      const currentErrorTimestamp = new Date().getTime();

      /**
       * Throw the error-event only in the following cases to avoid duplicate growls:
       * - if there was no previous error
       * - if the previous error occurred more than a second ago
       * - if the previous error had a different statuscode
       */
      if (!this.lastErrorReceivedTimestamp || currentErrorTimestamp - this.lastErrorReceivedTimestamp > 1000 || this.lastErrorReceived.status !== error.status) {
        // Add error to subject, this will show our error growl pop-up
        this.errorSubject.next({error});
      } else {
        console.log('Ignored the following error', error);
      }

      this.lastErrorReceived = error;
      this.lastErrorReceivedTimestamp = currentErrorTimestamp;

      // TODO Info requested from Guni square what we will do for what status code
      if (error.status === 404) {
        console.log('Not found ...');
      }
      if (error.status === 401 || error.status === 403) {
        console.log('Auth failure ...');
      }

    } else {

      alert(`Unkown error: ${error}`);

    }
  }
}
