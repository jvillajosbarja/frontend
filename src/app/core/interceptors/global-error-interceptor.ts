import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { GlobalErrorService } from '../services/error-handling/global-error.service';
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

/**
 * Global error handler interceptor.
 */
@Injectable()
export class GlobalErrorInterceptor implements HttpInterceptor {

  constructor(private globalErrorService: GlobalErrorService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(tap((event: HttpEvent<any>) => { // This needs to be a do operator or 5xx error won't fall through.
      return event; // return the original event object further down the chain
    }, (error: any) => {
      this.globalErrorService.handleError(error);

      // Return the original error so we can still catch them down the stream.
      // An example is provided in the CustomerDropdownComponent for this.customerService.getCustomersWithType
      return error;
    }));

  }

}
