import {
  HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';


import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

// Override serialization of the Date json object when mapping to JSON.
/* tslint:disable:no-invalid-this */
Date.prototype.toJSON = function() {
  return moment(this).format(HttpContentInterceptor.dateFormatter);
};

@Injectable()
export class HttpContentInterceptor implements HttpInterceptor {

  static dateFormatter: string = 'YYYY-MM-DDTHH:mm:ss';
  regexISO8601: RegExp = /^\d{4}(-[01]\d(-[0-3]\d(T[0-2]\d:[0-5]\d:?([0-5]\d(\.\d+‌​)?)?([+-][0-2]\d:[0-‌​5]\d)?[zZ]?)?)?)$/;
  headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // Always add the specified headers when doing requests to the api.
    // We always need to clone the request object when we are making changes to it as this is immutable
    // TODO Maybe we don't always want to add these headers for every request?
    req = req.clone({ headers: this.headers });

    // Handle the request and do something with the returned Http response
    return next.handle(req).pipe(map((event: HttpEvent<any>) => { // This needs to be a map operator !

      // Handle the returned HttpResponse object that is returned from our request.
      if (event instanceof HttpResponse) {
        // We always need to clone the HttpEvent object when we are making changes to it as this is immutable
        event = event.clone({body: this.deserializeHttpResponse(event.body)});
      }

      // We can handle other types of http responses here as defined in the HttpResponseBase abstract class
      // These include Sent, UploadProgress, ResponseHeader, DownloadProgress, Response and User events.

      return event;
    }));

  }

  /**
   * Deserialize the http response object.
   */
  protected deserializeHttpResponse(responseJson): HttpResponse<any> {
    return JSON.parse(JSON.stringify(responseJson), (key, value) => {
      if (typeof value === 'string' && value.match(this.regexISO8601)) {
        // use moment to parse ISO 8601 (in local time!)
        const milliseconds = moment(value, HttpContentInterceptor.dateFormatter).valueOf();
        if (!isNaN(milliseconds)) {
          return new Date(value);
        }
      }

      return value;
    });
  }

}
