import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './app.footer.component.html'
})
export class AppFooterComponent {

  appTitle: string = 'CSwebshop';
  appEmail: string = 'webshop@cipalschaubroeck.be';

}
