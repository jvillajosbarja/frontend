export interface Address {
    street: string;
    number: string;
    bus: string;
    zipcode: string;
    city: string;
    country: string;
}
