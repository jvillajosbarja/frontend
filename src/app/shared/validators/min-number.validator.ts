import { FormControl, ValidatorFn } from '@angular/forms';

export function createMinNumberValidator(minValue): ValidatorFn {
    // Deze functie geeft "null" terug indien valid, of een object indien invalid
    return function validateMinimumNumber(c: FormControl) {
        const num = c.value;
        // Bij een lege waarde geven we "valid" terug omdat we dit willen laten opvangen door de required-validator
        if (num === undefined || num === null) {
            return undefined;
        }
        if (isNaN(num) || num < minValue) {
            return {
                minValue: {given: num, min: minValue, valid: false}
            };
        }

        return undefined;
    };
}
