import { DocManagementState } from '../../docManagement/shared/models/docManagementState.model';


// Representation of the entire app state
// Extended by lazy loaded modules
export interface State {
  docManagement: DocManagementState;
}

