import { Component, Input, OnInit } from '@angular/core';
import { Address } from '../../models/address.model';

@Component({
  selector: 'app-address-column',
  templateUrl: './address-column.component.html',
  styleUrls: ['./address-column.component.scss']
})
export class AddressColumnComponent implements OnInit {

  @Input()
  address: Address;

  constructor() { }

  ngOnInit() {
  }

}
