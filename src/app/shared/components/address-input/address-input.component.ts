import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Address } from '../../models/address.model';

@Component({
  selector: 'app-address-input',
  templateUrl: './address-input.component.html',
  styleUrls: ['./address-input.component.scss']
})
export class AddressInputComponent implements OnInit, OnDestroy {

  @Input('parentForm')
  parentForm: FormGroup;

  addressForm: FormGroup;

  @Input()
  private addressFormControlName: string;

  private _address: Address;

  @Input()
  set address(address: Address) {
    this._address = address;

    // Vermijden van fout "Expression has changed" => SDM: vermoedelijk niet meer nodig na ARCH-613
    if (this.addressForm) {
      // this.cdr.detectChanges();
    }
  }

  get address(): Address {
    return this._address;
  }

  constructor(private fb: FormBuilder, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.addressForm = this.toFormGroup(this._address);
    this.parentForm.addControl(this.addressFormControlName, this.addressForm);
  }

  ngOnDestroy() {
    // this.parentForm.removeControl(this.addressFormControlName);
  }

  private toFormGroup(address: Address) {
    const formGroup = this.fb.group({
      street: [ (address && address.street) || '', Validators.required ],
      number: [ (address && address.number) || '', Validators.required ],
      bus: [ (address && address.bus) || ''],
      zipcode: [ (address && address.zipcode) || '', Validators.required ],
      city: [ (address && address.city) || '', Validators.required ],
      country: [ (address && address.country) || '', Validators.required ]
    });

    return formGroup;
  }

}
