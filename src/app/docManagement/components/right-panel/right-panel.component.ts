import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { DocumentProcess } from '../../shared/models/documentProcess.model';

@Component({
  selector: 'app-right-panel',
  templateUrl: './right-panel.component.html',
  styleUrls: ['./right-panel.component.scss']
})
export class RightPanelComponent implements OnChanges {
  @Input() selectedDocumentProcess: DocumentProcess;
  private documentProccess: DocumentProcess;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // patch form with value from the store
    if (changes.selectedDocumentProcess) {
      const documentProccess: any = changes.selectedDocumentProcess.currentValue as DocumentProcess;
      this.displayDocumentProcess(documentProccess);
    }

  }

  displayDocumentProcess(documentProccess: DocumentProcess | null): void {
    // Set the local product property
    this.documentProccess = documentProccess;
  }
}

