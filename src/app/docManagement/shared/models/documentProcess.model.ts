import { DocumentProcessType } from './documentProcessType.model';

export interface DocumentProcess {
  id: number | null;
  street: string;
  type: DocumentProcessType;
  creationDate: string;
  status: string;
  documentHandler: string;
  Attachments: string;
}
