import { TypeName } from '../enums/documentProcess.enum';

export interface DocumentProcessType {
  tabs: string;
  name: TypeName;
  documentBody: string;
  templateName: string;
}
