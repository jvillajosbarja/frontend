import { Action } from '@ngrx/store';
import { DocumentProcess } from '../../shared/models/documentProcess.model'


export enum DocumentProcessActionTypes {
  Load = '[DocumentProcess] Load',
  LoadSuccess = '[DocumentProcess] Load Success',
  LoadFail = '[DocumentProcess] Load Fail',
  UpdateDocumentProcess = '[DocumentProcess] Update DocumentProcess',
  UpdateDocumentProcessSuccess = '[DocumentProcess] Update DocumentProcess Success',
  UpdateDocumentProcessFail = '[DocumentProcess] Update DocumentProcess Fail',
  CreateDocumentProcess = '[DocumentProcess] Create DocumentProcess',
  CreateDocumentProcessSuccess = '[DocumentProcess] Create DocumentProcess Success',
  CreateDocumentProcessFail = '[DocumentProcess] Create DocumentProcess Fail',
  DeleteDocumentProcess = '[DocumentProcess] Delete DocumentProcess',
  DeleteDocumentProcessSuccess = '[DocumentProcess] Delete DocumentProcess Success',
  DeleteDocumentProcessFail = '[DocumentProcess] Delete DocumentProcess Fail',
  SetCurrentDocumentProcess = '[DocumentProcess] Set Current DocumentProcess',
}

// Action Creators
export class SetCurrentDocumentProcess implements Action {
  readonly type = DocumentProcessActionTypes.SetCurrentDocumentProcess;

  constructor(public payload: DocumentProcess) { }
}
export class Load implements Action {
  readonly type = DocumentProcessActionTypes.Load;
}

export class LoadSuccess implements Action {
  readonly type = DocumentProcessActionTypes.LoadSuccess;

  constructor(public payload: DocumentProcess[]) { }
}

export class LoadFail implements Action {
  readonly type = DocumentProcessActionTypes.LoadFail;

  constructor(public payload: string) { }
}

export class UpdateDocumentProcess implements Action {
  readonly type = DocumentProcessActionTypes.UpdateDocumentProcess;

  constructor(public payload: DocumentProcess) { }
}

export class UpdateDocumentProcessSuccess implements Action {
  readonly type = DocumentProcessActionTypes.UpdateDocumentProcessSuccess;

  constructor(public payload: DocumentProcess) { }
}

export class UpdateDocumentProcessFail implements Action {
  readonly type = DocumentProcessActionTypes.UpdateDocumentProcessFail;

  constructor(public payload: DocumentProcess) { }
}

export class CreateDocumentProcess implements Action {
  readonly type = DocumentProcessActionTypes.CreateDocumentProcess;

  constructor(public payload: string) { }
}

export class CreateDocumentProcessSuccess implements Action {
  readonly type = DocumentProcessActionTypes.CreateDocumentProcessSuccess;

  constructor(public payload: string) { }
}

export class CreateDocumentProcessFail implements Action {
  readonly type = DocumentProcessActionTypes.CreateDocumentProcessFail;

  constructor(public payload: string) { }
}

export class DeleteDocumentProcess implements Action {
  readonly type = DocumentProcessActionTypes.DeleteDocumentProcess;

  constructor(public payload: number) { }
}

export class DeleteDocumentProcessSuccess implements Action {
  readonly type = DocumentProcessActionTypes.DeleteDocumentProcessSuccess;

  constructor(public payload: number) { }
}

export class DeleteDocumentProcessFail implements Action {
  readonly type = DocumentProcessActionTypes.DeleteDocumentProcessFail;

  constructor(public payload: string) { }
}

// Union the valid types
export type DocumentProcessActions = Load
  | LoadSuccess
  | SetCurrentDocumentProcess
  | LoadFail
  | UpdateDocumentProcess
  | UpdateDocumentProcessSuccess
  | UpdateDocumentProcessFail
  | CreateDocumentProcess
  | CreateDocumentProcessSuccess
  | CreateDocumentProcessFail
  | DeleteDocumentProcess
  | DeleteDocumentProcessSuccess
  | DeleteDocumentProcessFail;

