import { createFeatureSelector, createSelector } from '@ngrx/store';
import { DocManagementState } from '../../shared/models/docManagementState.model';
import { DocumentProcessType } from '../../shared/models/documentProcessType.model';
import { TypeName } from '../../shared/enums/documentProcess.enum';

// Selector functions
const getProductFeatureState = createFeatureSelector<DocManagementState>('docManagement');


export const getDocumentProcessList = createSelector(
  getProductFeatureState,
  state => state.documentProcessList
);

export const getDocumentProcessId = createSelector(
  getProductFeatureState,
  state => state.currentDocumentProcessId
);

const emailtype: DocumentProcessType = {
  'tabs':'tabs emailtype',
  'name': TypeName.EMAIL,
  'documentBody':'documentBody ded',
  'templateName':'templateName'
};
export const getDocumentProcess = createSelector(
  getProductFeatureState,
  getDocumentProcessId,
  (state, currentDocumentProcessId) => {
    if (currentDocumentProcessId === 0) {
      return {
      id: null,
      street: '',
      type: emailtype,
      creationDate: '',
      status:  '',
      documentHandler:  '',
      Attachments: ''
      };
    } else {
      return currentDocumentProcessId ? state.documentProcessList.find(p => p.id === currentDocumentProcessId) : null;
    }
  }
);



export const getError = createSelector(
  getProductFeatureState,
  state => state.error
);
