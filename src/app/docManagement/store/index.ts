import * as fromRoot from '../../shared/store/app.state';
//import * as DocManagementReducer from './reducers/docManagement.reducer';
import * as DocManagementActions from './actions/docManagement.action'
import * as DocManagementSelectors from './selectors/docManagement.selector'
import { DocManagementState } from '../shared/models/docManagementState.model';

// Extends the app state to include the  DocManagement feature.
// This is required because DocManagement are lazy loaded.
// So the reference to DocManagementState cannot be added to app.state.ts directly.
export interface State extends fromRoot.State {
  products: DocManagementState;
}

export{
  DocManagementSelectors,
  DocManagementActions
}
