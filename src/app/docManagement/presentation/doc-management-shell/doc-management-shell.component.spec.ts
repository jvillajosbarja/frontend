import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocManagementShellComponent } from './doc-management-shell.component';

describe('DocManagementShellComponent', () => {
  let component: DocManagementShellComponent;
  let fixture: ComponentFixture<DocManagementShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocManagementShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocManagementShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
