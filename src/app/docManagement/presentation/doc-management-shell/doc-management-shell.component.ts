import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import * as fromDoc from '../../store/index';
import { DocumentProcess } from '../../shared/models/documentProcess.model';



@Component({
  templateUrl: './doc-management-shell.component.html',
  styleUrls: ['./doc-management-shell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocManagementShellComponent implements OnInit {

  selectedDocumentProcess$: Observable<DocumentProcess>;
  documentProcessList$: Observable<DocumentProcess[]>;
  errorMessage$: Observable<string>;

  constructor(private store: Store<fromDoc.State>) {}

  ngOnInit(): void {
    this.store.dispatch(new fromDoc.DocManagementActions.Load());
    this.documentProcessList$ = this.store.pipe(select(fromDoc.DocManagementSelectors.getDocumentProcessList));
    this.errorMessage$ = this.store.pipe(select(fromDoc.DocManagementSelectors.getError));
    this.selectedDocumentProcess$ = this.store.pipe(select(fromDoc.DocManagementSelectors.getDocumentProcess));
  }

  productSelected(documentProcess: DocumentProcess): void {
    console.log('productSelected');
    this.store.dispatch(new fromDoc.DocManagementActions.SetCurrentDocumentProcess(documentProcess));
  }

  productUnSelected(documentProcess: DocumentProcess) {
    // implement clear store
  }
}
